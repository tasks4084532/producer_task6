package org.ms.pojo;

import lombok.Data;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Setter
@Entity
@Table(name = "statusrcoi")
public class StatusRcoi {
    @Id
    private long id;

    @Column(name = "servicenumber")
    private String serviceNumber;

    @Column(name = "statuscode")
    private String statusCode;

    @Column(name = "created")
    private String created;

    @Column(name = "msgid")
    private String msgId;

    @Column(name = "put")
    private String put;

    @Column(name = "reasoncode")
    private String reasonCode;

    @Column(name = "route")
    private boolean route;

    @Column(name = "direction")
    private String direction;

    @Column(name = "status")
    private String status;
}
