package org.ms.props;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("thread")
public class ThreadProps {
    Integer countOfThreads;
}
