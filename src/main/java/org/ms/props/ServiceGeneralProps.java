package org.ms.props;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("service")
public class ServiceGeneralProps {
    private String apiTitle;
    private String apiVersion;
    private String apiDescription;
}
