package org.ms.repos;

import org.ms.pojo.StatusRcoi;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface CsvRepo extends CrudRepository<StatusRcoi, Long> {
    @Modifying
    @Query(value = "TRUNCATE TABLE statusrcoi;", nativeQuery = true)
    @Transactional
    void truncateDatabase();
}