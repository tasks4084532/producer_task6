package org.ms.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.message.MapMessage;
import org.ms.pojo.StatusRcoi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;



@Slf4j
@Service
@RequiredArgsConstructor
public class MessageSender {

    private final JmsTemplate jmsTemplate;

    @Value("${queue}")
    private String queue;

    public void send(StatusRcoi statusRcoi) {
        try {
            log.info("Try to send msg to address: {}", queue);
            sendMsg(statusRcoi);
            log.info("Send msg to {} successfully", queue);
        } catch (Exception exception) {
            log.error("Error while sending " + statusRcoi + " to AMQ", exception);
            throw exception;
        }
    }

    private void sendMsg(StatusRcoi statusRcoi) {
        jmsTemplate.convertAndSend(queue, statusRcoi);
//        jmsTemplate.send(queue, session -> {
//            MapMessage msg = session.createMapMessage();
//
//            msg.setLong("id", statusRcoi.getId());
//            msg.setString("serviceNumber", statusRcoi.getServiceNumber());
//            msg.setString("statusCode", statusRcoi.getStatusCode());
//            msg.setString("msgId", statusRcoi.getMsgId());
//            msg.setString("put", statusRcoi.getPut());
//            msg.setString("reasonCode", statusRcoi.getReasonCode());
//            msg.setBoolean("route", statusRcoi.isRoute());
//            msg.setString("direction", statusRcoi.getDirection());
//            msg.setString("status", "SENT");
//
//            return msg;
//        });
    }

}
