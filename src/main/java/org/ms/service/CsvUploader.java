package org.ms.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ms.pojo.StatusRcoi;
import org.ms.props.ThreadProps;
import org.ms.repos.CsvRepo;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseBool;
import org.supercsv.cellprocessor.ParseLong;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@RequiredArgsConstructor
public class CsvUploader {

    private final CsvRepo csvRepo;
    private final ThreadProps threadProps;
    private final MessageSender messageSender;

    public List<StatusRcoi> csvParser(MultipartFile file) throws IOException {
        try (ICsvBeanReader beanReader
                     = new CsvBeanReader(new InputStreamReader(file.getInputStream()), CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE)) {

            final String[] headers = beanReader.getHeader(true);
            final CellProcessor[] processors = getProcessors();

            StatusRcoi statusRcoi;

            ArrayList<StatusRcoi> allStatuses = new ArrayList<>();
            while ((statusRcoi = beanReader.read(StatusRcoi.class, headers, processors)) != null) {
                statusRcoi.setStatus("SENT");
                allStatuses.add(statusRcoi);
            }
            return allStatuses;
        }
    }

    private CellProcessor[] getProcessors() {
        return new CellProcessor[]{
                new NotNull(new ParseLong()), // id
                new NotNull(), //serviceNumber
                new NotNull(), // statusCode
                new NotNull(), // created
                new NotNull(), // msgId
                new NotNull(), // put
                new Optional(), // reasonCode
                new NotNull(new ParseBool()), // route
                new NotNull() // direction
        };
    }

    public class saveToDatabase implements Runnable {
        StatusRcoi status;

        public saveToDatabase(StatusRcoi status) {
            this.status = status;
        }

        @Override
        public void run() {
            csvRepo.save(status);
            messageSender.send(status);
        }
    }

    public void saveToDatabaseMultithreading(MultipartFile file) throws IOException, InterruptedException {
        List<StatusRcoi> statuses = csvParser(file);
        long startTime = System.nanoTime();

        log.info("Count of threads: " + threadProps.getCountOfThreads());

        ExecutorService executorService = Executors.newFixedThreadPool(threadProps.getCountOfThreads());
        for (StatusRcoi status : statuses) {
            executorService.execute(new saveToDatabase(status));
        }

        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

        long endTime = System.nanoTime();
        long duration = (endTime - startTime) / 1000000;
        log.info("Saving time: " + duration + " ms"); //537504 ms without thread

    }
}
