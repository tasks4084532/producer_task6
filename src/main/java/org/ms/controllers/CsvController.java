package org.ms.controllers;

import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ms.repos.CsvRepo;
import org.ms.responses.OnlyMessageResponse;
import org.ms.service.CsvUploader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Slf4j
@RestController
@Api(tags = "Controllers")
@RequiredArgsConstructor
public class CsvController {

    private final CsvUploader csvUploader;
    private final CsvRepo csvRepo;

    @CrossOrigin
    @PostMapping(path = "/csv-update")
    @ApiOperation(value = "Загружает csv файл")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Файл загружен успешно", response = OnlyMessageResponse.class),
//            @ApiResponse(code = 400, message = "Ошибка запроса"),
//            @ApiResponse(code = 409, message = "Конфликт"),
//            @ApiResponse(code = 422, message = "Невозможно обработать данные"),
            @ApiResponse(code = 500, message = "Внутренняя ошибка сервиса")
    }
    )
    public ResponseEntity<OnlyMessageResponse> postCsv(
            @ApiParam(name = "file", value = "Файл", required = true)
            @RequestParam("file")
            MultipartFile file
    ) throws IOException, InterruptedException {
        log.info("/csv-update called");

        csvUploader.saveToDatabaseMultithreading(file);

        log.info("/csv-update answered");
        return new ResponseEntity<>(new OnlyMessageResponse("Csv uploaded"), HttpStatus.OK);
    }

    @PostMapping(path = "/truncate")
    @ApiOperation(value = "Чистит табличку")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Очищено успешно", response = OnlyMessageResponse.class),
//            @ApiResponse(code = 400, message = "Ошибка запроса"),
//            @ApiResponse(code = 409, message = "Конфликт"),
//            @ApiResponse(code = 422, message = "Невозможно обработать данные"),
            @ApiResponse(code = 500, message = "Внутренняя ошибка сервиса")
    }
    )
    public ResponseEntity<OnlyMessageResponse> truncate(
    ) throws IOException {
        log.info("/truncate called");

        csvRepo.truncateDatabase();

        log.info("/truncate answered");
        return new ResponseEntity<>(new OnlyMessageResponse("Csv uploaded"), HttpStatus.OK);
    }
}
