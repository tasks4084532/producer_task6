package org.ms;

import org.ms.props.ServiceGeneralProps;
import org.ms.props.ThreadProps;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.jms.core.JmsTemplate;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//@EnableJms
@EnableSwagger2
@SpringBootApplication
@ServletComponentScan
@EnableConfigurationProperties({
        ServiceGeneralProps.class,
        ThreadProps.class})
//@Import(TracingJmsConfiguration.class)
public class RestApplication {
    public static void main(String[] args) {
        SpringApplication.run(RestApplication.class, args);
    }
}